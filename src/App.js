import React, { Component } from "react";
import Navbar from "./Components/Navbar";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import HomePage from "./Components/HomePage";
import About from "./Components/About";
import Shop from "./Components/Shop";
import CalculatorPage from "./Components/CalculatorPage";
import Contact from "./Components/Contact";
import PaymentAndDelivery from "./Components/PaymentAndDelivery";
import { connect } from "react-redux";
import "./scss/App.scss";
import Footer from "./Components/Footer";
import Cart from "./Components/Cart";
import ProductDescription from "./Components/ProductDescription";
import OrderDone from "./Components/ChildComponents/Orderdone";

class App extends Component {
	render() {
		return (
			<BrowserRouter>
				<div className="App">
					<Navbar />
					<Switch>
						<Route path="/home" component={HomePage} />
						<Route path="/about" component={About} />
						<Route path="/wholesaleCalculator" component={CalculatorPage} />
						<Route path="/orderDone" component={OrderDone} />
						<Route exact path="/shop" component={Shop} />
						<Route path="/shop/product" component={ProductDescription} />
						<Route path="/contact" component={Contact} />
						<Route path="/paymentAndDelivery" component={PaymentAndDelivery} />
						<Route path="/cart" component={Cart} />
					</Switch>
					<Footer />
				</div>
			</BrowserRouter>
		);
	}
}
const mapStateToProps = state => ({
	productsCustom: state.products
});

export default connect(mapStateToProps)(App);
