import baseUrl from "../lib/baseUrl";

const fetchProductInfo = productArticle => {
	return dispatch => {
		dispatch({
			type: "FETCH_INFO_START"
		});
		return fetch(`${baseUrl}product/${productArticle}`, {
			method: "GET",
			headers: {
				"Content-Type": "application/json",
				"Access-Control-Allow-Origin": "*"
			}
			// body: JSON.stringify({
			// 	order: articlesArray,
			// 	phone_number: "063 527 19 87",
			// 	name: "Yurec"
			// })
		})
			.then(resp => resp.json())
			.then(data => {
				dispatch({
					type: "FETCH_INFO_COMPLETED",
					payload: data
				});
			})
			.catch(err => {
				dispatch({
					type: "FETCH_INFO_ERR",
					payload: err
				});
				console.log(err);
			});
	};
};
export default fetchProductInfo;
