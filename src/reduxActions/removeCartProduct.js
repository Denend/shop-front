const removeCartProduct = (productArticle, size) => (dispatch, getState) => {
	const currentCart = getState().productCart.cartProducts;
	const newCart =
		currentCart &&
		currentCart.filter(
			product =>
				product.sizes[0] !== size || product.article !== productArticle,
		);
	dispatch({
		type: "REMOVE_PRODUCT_FROM_CART",
		payload: { cartProducts: [...newCart] },
	});
};
export default removeCartProduct;
