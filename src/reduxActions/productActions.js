import * as R from "rambda";
import { notySuccess, notyInfo } from "../Components/ChildComponents/noty";

const checkIfInCart = (ProductId, size, getState) =>
	getState().productCart.cartProducts.every(element => {
		return element.article !== parseInt(ProductId) || element.sizes[0] !== size;
	});

const updateProductCart = (ProductId, size) => (dispatch, getState) => {
	const allShopProducts = getState().shopProducts.goodsArray.products_data;
	const productToAdd = R.find(R.propEq("article", parseInt(ProductId)))(
		allShopProducts,
	);

	if (checkIfInCart(ProductId, size, getState)) {
		const payload = size
			? [{ ...productToAdd, sizes: [size] }]
			: [{ ...productToAdd }];
		dispatch({
			type: "ADD_PRODUCT_TO_CART",
			payload,
		});
		notySuccess("Товар добавлен в корзину");
	} else notyInfo("Товар уже в корзине");
};
export default updateProductCart;
