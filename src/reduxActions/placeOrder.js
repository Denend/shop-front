import { notySuccess, notyError } from "../Components/ChildComponents/noty";
import baseUrl from "../lib/baseUrl";

const placeOrder = (articlesArray, userName, userPhoneNumber) => {
	return dispatch => {
		return fetch(`${baseUrl}order`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Access-Control-Allow-Origin": "*",
			},
			body: JSON.stringify({
				items: articlesArray,
				phone_number: userPhoneNumber,
				name: userName,
			}),
		})
			.then((resp, reject) => {
				if (resp.status === 409) {
					resp.json().then(errData => {
						dispatch({
							type: "PLACE_ORDER_ERROR",
							payload: { notAvailable: errData.message },
						});
						notyError("Часть товаров отсутствует на складе");
					});
				}

				return resp.json();
			})
			.then(data => {
				// console.log(data);
				dispatch({
					type: "PLACE_ORDER",
					payload: { orderInfo: data },
				});
				notySuccess("Ваш заказ успешно оформлен");
				window.location.replace("/orderDone");
			})
			.catch(err => {
				// err.then(errData => {
				// 	console.log(errData);
				// 	notySuccess("Часть товаров отсутствует на складе");
				// });
				// console.log(err);
				notyError("Упс, что-то пошло нетак");
			});
	};
};
export default placeOrder;
