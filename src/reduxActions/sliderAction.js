import * as axios from "axios";
import baseUrl from "../lib/baseUrl";
const TOGLE_IS_FETCHING = "TOGLE_IS_FETCHING";

const SET_USERS = "SET_USERS";
const SET_CURRENT_PAGE = "SET_CURRENT_PAGE";

export const sliderAPI = {
	getSlider() {
		return axios
			.get(`${baseUrl}products?per_page=6&discount=1`)
			.then(response => {
				return response.data;
			});
	}
};

export const setproducts_dataAC = products_data => ({
	type: SET_USERS,
	products_data
});
export const setcurrentPageAC = currentPage => ({
	type: SET_CURRENT_PAGE,
	currentPage: currentPage
});
export const togleIsFetchingAC = isFetching => ({
	type: TOGLE_IS_FETCHING,
	isFetching: isFetching
});

export const getSliderThunk = () => {
	return dispatch => {
		dispatch(togleIsFetchingAC(true));
		sliderAPI.getSlider().then(data => {
			dispatch(togleIsFetchingAC(false));
			dispatch(setproducts_dataAC(data.products_data));
		});
	};
};
