const fetchProducts = (get, url) => dispatchShit => {
	get(url)
		.then(response => {
			const resData = JSON.parse(response);
			dispatchShit({
				type: "FETCH_GOODS_COMPLETED",
				payload: resData
			});
		})
		.catch(err => {
			dispatchShit({ type: "FETCH_GOODS_ERR", payload: err });
		});
};
export default fetchProducts;
