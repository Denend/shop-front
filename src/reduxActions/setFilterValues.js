export function setFilterValues(updatedValues) {
	return {
		type: "SET_FILTER_VALUES",
		payload: {
			...updatedValues
		}
	};
}

export function resetFilterValues(updatedValues) {
	return {
		type: "RESET_FILTERS"
	};
}
