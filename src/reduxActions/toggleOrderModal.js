export const toggleOrderModal = () => (dispatch, getState) => {
  dispatch({
    type: "TOGGLE_CART_MODAL",
    payload: !getState().productCart.isModalOpen
  });
};
