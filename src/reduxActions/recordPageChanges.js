const recordPageChanges = currentPage => {
	return {
		type: "RECORD_CURRENT_PAGE",
		payload: { currentPage }
	};
};

export default recordPageChanges;
