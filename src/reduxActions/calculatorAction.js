import * as axios from "axios";
import { notySuccess, notyError } from "../Components/ChildComponents/noty";
import baseUrl from "../lib/baseUrl";
const GO_BASKET = "GO_BASKET";
const TOGLE_IS_ERROR = "TOGLE_IS_ERROR";
const TOGLE_IS_MODAL = "TOGLE_IS_MODAL";
const TOGLE_IS_FETCHING = "TOGLE_IS_FETCHING";
const SET_BREND = "SET_BREND";
const SET_Q_BRAND = "SET_Q_BRAND";
const SET_CATEGORIES = "SET_CATEGORIES";
const SET_PRICE = "SET_PRICE";
const SET_BRENDS = "SET_BRENDS";
const SET_WEIGHT = "SET_WEIGHT";
const SET_REZULT = "SET_REZULT";
const SET_BASKET = "SET_BASKET";
const DELETE_BASKET_ITEM = "DELETE_BASKET_ITEM";
const SET_CATEGORIES_API = "SET_CATEGORIES_API";

export const CalculatorAPI = {
  getCalculatorInfo() {
    return axios.get(`${baseUrl}wholesale_calc`).then(response => {
      return response.data;
    });
  },
  putBasket(orderApi, categoriesApi, phone_numberApi, nameApi, commentApi) {
    return fetch(`${baseUrl}wholesale_order`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"
      },
      body: JSON.stringify({
        order: orderApi,
        categories: categoriesApi,
        phone_number: phone_numberApi,
        name: nameApi,
        comment: commentApi
      })
    })
      .then(resp => resp.json())
      .then(response => {
        console.log(response.message);
        if (response.message === undefined) {
          notySuccess("Ваш заказ успешно оформлен");
          window.location.replace("/orderDone");
        } else {
          notyError("Упс, что-то пошло нетак");
        }
      })
      .catch(error => {
        console.log(error.response.data);
      });
  }
};
export const setBrends = brends => ({ type: SET_BRENDS, brends });
export const setCategories = categories => ({
  type: SET_CATEGORIES,
  categories
});
export const setCategoriesAPI = categoriesAPI => ({
  type: SET_CATEGORIES_API,
  categoriesAPI
});

export const setBasket = () => ({ type: SET_BASKET });
export const setBrend = brend => ({ type: SET_BREND, brend });
export const setQBrand = qBrand => ({ type: SET_Q_BRAND, qBrand });

export const setWeight = weight => ({ type: SET_WEIGHT, weight });
export const setPrice = price => ({ type: SET_PRICE, price });
export const setResult = result => ({ type: SET_REZULT, result: result });
export const togleIsFetching = isFetching => ({
  type: TOGLE_IS_FETCHING,
  isFetching: isFetching
});
export const toggleGoBasket = goBasket => ({
  type: GO_BASKET,
  goBasket: goBasket
});
export const toggleModal = isModalOpen => ({
  type: TOGLE_IS_MODAL,
  isModalOpen: isModalOpen
});
export const toggleError = isError => ({
  type: TOGLE_IS_ERROR,
  isError: isError
});
export const deleteBasketItem = productArticle => (dispatch, getState) => {
  const currentCart = getState().calculatorComponent.basket;
  const newCart =
    currentCart && currentCart.filter(product => product.id !== productArticle);
  dispatch({
    type: DELETE_BASKET_ITEM,
    payload: { basket: [...newCart] }
  });
};

export const getCalculatorThunk = () => {
  return dispatch => {
    dispatch(togleIsFetching(true));
    CalculatorAPI.getCalculatorInfo().then(data => {
      dispatch(togleIsFetching(false));
      dispatch(setBrends(data.brands));
      dispatch(setCategories(data.categories));
    });
  };
};
