const get = url => {
	return new Promise(function(resolve, reject) {
		let httpRequest = new XMLHttpRequest();
		// httpRequest.overrideMimeType("application/json");
		httpRequest.open("GET", url);
		// httpRequest.setRequestHeader(
		//   "Content-type",
		//   "application/json; charset=utf-8"
		// );
		httpRequest.onload = () => {
			if (httpRequest.status === 200) {
				resolve(httpRequest.response);
			} else {
				reject(Error(httpRequest.statusText));
			}
		};
		httpRequest.onerror = () => {
			reject(Error("Network Error"));
		};
		httpRequest.send();
	});
};

export default get;
