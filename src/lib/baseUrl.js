const envUrl = process.env.REACT_APP_SERVER_URL;
const localHost = "http://127.0.0.1:5000/";
const baseUrl = envUrl ? envUrl : localHost;
export default baseUrl;
