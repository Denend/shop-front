import React, {Component} from 'react';


class Footer extends Component {
  render(){
    return(
      <footer className = 'footerWrapper'>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"></link>
        <div className = 'FooterInfo'>
          <h3>The best e-shop vseya rusi</h3>
          <p>+48732863542</p>
          <p>Пн - Пт с 11 до 19</p>
          <p>© 2019 CITY HERO™</p>
        </div>
        <div className = 'socialIconsContainer'>
            <a href="https://www.facebook.com/daniel.soloviov.3" className = "fa fa-facebook"> </a>
            <a href="/bvb" className = "fa fa-instagram"> </a>
            <a href="/bvb" className = "fa fa-telegram"> </a>
        </div>
      </footer>
    )
  }
}
export default Footer;
