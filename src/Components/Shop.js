import React, { Component } from "react";
import ReactPaginate from "react-paginate";
import ShopFilters from "./ChildComponents/ShopFilters";
import { connect } from "react-redux";
import updateProductCart from "../reduxActions/productActions";
import get from "../lib/requests";
import fetchProductsAction from "../reduxActions/fetchProductsAction";
import recordPageChanges from "../reduxActions/recordPageChanges";
import * as R from "ramda";
import baseUrl from "../lib/baseUrl";
import SelelectFilter from "../Components/ChildComponents/SelectFilter";
import ShopProduct from "../Components/ChildComponents/ShopProduct";

class Shop extends Component {
	state = {
		areFiltersOpen: false,
	};
	componentWillMount() {
		this.props.fetchProductsAction(get, this.getUrlPagination(1));
	}

	componentDidUpdate(prevProps) {
		//fetches products when filters are touched
		const {
			updatedFilterValues,
			fetchProductsAction,
			currentPaginationPage,
		} = this.props;
		if (
			!this.compareObjects(updatedFilterValues, prevProps.updatedFilterValues)
		) {
			fetchProductsAction(get, this.getUrlPagination(currentPaginationPage));
		}
	}

	compareObjects = (obj1, obj2) =>
		JSON.stringify(obj1) === JSON.stringify(obj2);

	UrlToProductDesc = prodId => {
		this.props.history.push(`/shop/product/?article=${prodId}`); //passing product id throught url params;
	};

	onUpdateProduct = (e, size) => {
		this.props.onUpdateProduct(e.currentTarget.name, size);
	};

	// gets a string to add to url from checked filters in Redux
	composeUrlFromFilters = () => {
		const { updatedFilterValues } = this.props;
		return Object.keys(updatedFilterValues)
			.map(key => {
				const filterOfCurrentType = updatedFilterValues[key];
				return Object.keys(R.pickBy(value => value, filterOfCurrentType)).map(
					filter => `${key}=${filter}`,
				);
			})
			.map(cleanFilterTypes => cleanFilterTypes.join("&"))
			.filter(array => array)
			.join("&");
	};

	getUrlPagination = pageNumber =>
		`${baseUrl}products?page=${pageNumber}&per_page=30&${this.composeUrlFromFilters()}`; //per page statys hardcoded for a now

	onPaginationClick = obj => {
		const currPageNumber = obj.selected + 1;
		this.props.fetchProductsAction(get, this.getUrlPagination(currPageNumber));
		this.props.recordPageChanges(obj.selected + 1);
		window.scrollTo(0, 0);
	};

	getNumbeOfPages = perPage =>
		this.props.shopProducts.goodsArray.items_total / perPage;

	toggleFilters = () => {
		this.setState({
			areFiltersOpen: !this.state.areFiltersOpen,
		});
	};

	render() {
		const { updatedFilterValues, shopProducts, isDesktop } = this.props;
		return (
			<div className='shopWrapper container-standart'>
				{!isDesktop && !this.state.areFiltersOpen ? null : (
					<ShopFilters
						reduxFilterValues={updatedFilterValues}
						toggleFilters={this.toggleFilters}
						isDesktop={isDesktop}
					/>
				)}
				<div
					className={`shelvesAndPaginationWrapper${
						this.state.areFiltersOpen ? "-hidden" : ""
					}`}
				>
					<div className='shopHeader'>
						<div className='selectFilters'>
							<SelelectFilter
								className='filterByPrice'
								reduxValues={updatedFilterValues}
								filterType='price'
							/>
							<SelelectFilter
								className='filterByData'
								reduxValues={updatedFilterValues}
								filterType='date'
							/>
						</div>
						{!isDesktop && (
							<button
								className='shopHeader-showFilters'
								onClick={() => this.toggleFilters()}
							>
								Дополнительные фильтры
							</button>
						)}
					</div>
					<div className='ShopShelvesDiv'>
						{shopProducts.goodsArray.products_data &&
							shopProducts.goodsArray.products_data.map((elem, index) => {
								return (
									<ShopProduct
										key={index}
										index={index}
										elem={elem}
										onUpdateProduct={this.onUpdateProduct}
										UrlToProductDesc={this.UrlToProductDesc}
									/>
								);
							})}
					</div>
					<ReactPaginate
						activeClassName='activePaginationCell'
						pageCount={this.getNumbeOfPages(30)}
						pageRangeDisplayed={2}
						marginPagesDisplayed={1}
						previousLabel={null}
						nextLabel={null}
						containerClassName='paginationContainer'
						pageClassName='paginationCells'
						pageLinkClassName='paginationCellLinks'
						breakClassName='paginationEllipsis'
						onPageChange={this.onPaginationClick}
					/>
				</div>
			</div>
		);
	}
}
const mapStateToProps = state => ({
	products: state.productCart,
	shopProducts: state.shopProducts,
	updatedFilterValues: state.updatedFilterValues,
	currentPaginationPage: state.pagination.currentPage,
	isDesktop: state.breakpoints.isDesktop,
});

const mapDispatchToProps = dispatch => ({
	fetchProductsAction: (get, url) => dispatch(fetchProductsAction(get, url)),
	onUpdateProduct: (ProductId, size) =>
		dispatch(updateProductCart(ProductId, size)),
	recordPageChanges: currPage => dispatch(recordPageChanges(currPage)),
});

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(Shop);
