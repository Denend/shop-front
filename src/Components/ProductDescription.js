import React, { Component } from "react";
import updateProductCart from "../reduxActions/productActions";
import fetchProductInfo from "../reduxActions/fetchProductInfo";
import { connect } from "react-redux";
import Gallery from "./ChildComponents/Gallery";
import { notyInfo } from "../Components/ChildComponents/noty";

class ProductDescription extends Component {
	state = {
		selectedSize: null,
	};

	componentWillMount() {
		this.props.fetchProductInfo(this.getArticleFromUrl());
	}

	getArticleFromUrl = () => {
		const url = new URL(window.location.href);
		return url.searchParams.get("article");
	};

	handleSizeClick = e => {
		e.currentTarget.parentNode.childNodes.forEach(e =>
			e.classList.remove("active"),
		);
		e.currentTarget.classList.add("active");
		this.setState({ selectedSize: e.currentTarget.id });
	};

	checkIfSizeAvailable = sizes =>
		this.state.selectedSize &&
		sizes.some(elem => elem.size === this.state.selectedSize);

	handleAddClick = sizes => {
		if (this.checkIfSizeAvailable(sizes)) {
			const size = this.state.selectedSize;
			this.props.onUpdateProduct(this.getArticleFromUrl(), size);
		} else if (!this.state.selectedSize) {
			notyInfo("Выберите размер");
		} else {
			notyInfo("Этого размера нет в наличии");
		}
	};

	render() {
		const { productInfo } = this.props;
		const {
			article,
			brand,
			name,
			category,
			discount_price,
			images,
			description,
			sizes,
			price,
		} = this.props.productInfo.productInfo;

		const sizesArray = ["S", "M", "L", "XL", "XXL"];
		return productInfo.fetching ? (
			<div>Loading...</div> // TO DO add Arturs spinner here
		) : (
			<div className='ProductDescriptionMainWrapper container-standart'>
				<div className='ProductDescriptionWrapper'>
					<div className='DescriptionGalleryWrapper'>
						<Gallery images={images} />
					</div>
					<div className='DescriptionSideInfo'>
						<div className='DescriptionSideInfo-header'>
							<h3>{name}</h3>
							<div className='pathWrapper'>
								<a className='path' path='exact' href={"/shop"}>
									Магазин/
								</a>
								<a className='path' href={window.location.href}>
									Информация о товаре
								</a>
							</div>
							<p className='productArticle'>Артикул &#8470; {article}</p>
							<p
								className={`${
									discount_price > 0 ? "price-discounted" : "price"
								}`}
							>
								{discount_price > 0 ? discount_price : price} ₴
							</p>
							{discount_price > 0 && <p className='discount'>{`${price} ₴`}</p>}
						</div>
						<hr />
						<h3>Размер</h3>
						<ul className='DescriptionSideInfo-sizesContainer'>
							{sizesArray.map((size, index) => {
								return sizes && sizes.some(elem => elem.size === size) ? (
									<li
										key={index}
										onClick={this.handleSizeClick}
										className='isAvailable'
										id={size}
									>
										{size}
									</li>
								) : (
									<li
										key={index}
										onClick={this.handleSizeClick}
										className='notAvailable'
										id={size}
									>
										{size}
									</li>
								);
							})}
						</ul>
						<hr />
						<button
							type='button'
							className='toCartButton'
							onClick={e => this.handleAddClick(sizes)}
						>
							В Корзину
						</button>
						<p>Расскажи о товаре друзьям</p>
					</div>
				</div>
				<div className='ProductDescriptionInfo'>
					<p>{description}</p>
					<p>Бренд: {brand}</p>
				</div>
				{/* <div className="ProductDescriptionRecommendations"></div> */}
			</div>
		);
	}
}

const mapStateToProps = state => ({
	productInfo: state.productInfo,
});
const mapDispatchToProps = dispatch => ({
	onUpdateProduct: (ProductId, size) =>
		dispatch(updateProductCart(ProductId, size)),
	fetchProductInfo: ProductId => dispatch(fetchProductInfo(ProductId)),
});

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(ProductDescription);
