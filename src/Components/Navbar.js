import React, { useState } from "react";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import AnimateHeight from "react-animate-height";
import NavbarLower from "./ChildComponents/NavbarLower";
import NavbarUpper from "./ChildComponents/NavbarUpper";

const Navbar = props => {
	const changeLocationCart = () => {
		props.history.push("/cart");
	};

	const { products, isDesktop } = props;
	const [menuHeight, setMenuHeight] = useState(60);
	return (
		<div className="NavbarDiv">
			<nav>
				{!isDesktop ? (
					<AnimateHeight height={menuHeight} duration={500}>
						<NavbarUpper
							isDesktop={isDesktop}
							changeLocationCart={changeLocationCart}
							setMenuHeight={setMenuHeight}
							menuHeight={menuHeight}
							products={products}
						/>
						<NavbarLower isDesktop={isDesktop} />
					</AnimateHeight>
				) : (
					<>
						<NavbarUpper
							isDesktop={isDesktop}
							changeLocationCart={changeLocationCart}
							setMenuHeight={setMenuHeight}
							menuHeight={menuHeight}
							products={products}
						/>

						<NavbarLower isDesktop={isDesktop} />
					</>
				)}
			</nav>
		</div>
	);
};
const mapStateToProps = state => ({
	products: state.productCart.cartProducts,
	isDesktop: state.breakpoints.isDesktop
});

export default connect(mapStateToProps)(withRouter(Navbar));
