import React from "react";

const Checkbox = ({
	name,
	checked,
	value,
	onChange,
	onBlur,
	// form: { errors, touched, setFieldValue },
	id,
	label,
	className
}) => {
	return (
		<div className="checkbox-wrapper">
			<input
				name={name}
				id={id}
				type="checkbox"
				value={value}
				checked={checked}
				onChange={onChange}
				onBlur={onBlur}
				className="checkBox"
			/>
			<label htmlFor={id}>{label}</label>
		</div>
	);
};
export default Checkbox;
