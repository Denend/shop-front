import React from "react";

export const FormControl = ({ input, meta, child, ...props }) => {
  const hasError = meta.touched && meta.error;
  return (
    <div >
      <div>
       {props.children}
      </div>
      {hasError && <span>Введите ваше имя</span>}
    </div>
  );
};

export const InputAreaName = ({ input, meta, ...props }) => {
  const hasError = meta.touched && meta.error;
  return (
    <div className={`formControl${hasError ? "-error" : ""}`}>
      <div>
        <input {...input} {...props} />
      </div>
      {hasError && <span>Введите ваше имя</span>}
    </div>
  );
};
export const InputAreaNumber = ({ input, meta, ...props }) => {
  const hasError = meta.touched && meta.error;
  return (
    <div className={`formControl${hasError ? "-error" : ""}`}>
      <div>
        <input  {...input} {...props} />
      </div>
      {hasError && <span>Введите ваш номер телефона</span>}
    </div>
  );
};
export const InputAreaCategories1 = ({ input, meta, ...props }) => {
  const hasError = meta.touched && meta.error;
  return (
    <div className={`formControl${hasError ? "-error" : ""}`}>
       <div>
        <input  {...input} {...props} />
      </div>
      {hasError && <span>Выберите категорию </span>}
    </div>
  );
};
export const InputAreaCategories = (props) => {
  const{ input, meta, child, ...restProps } =props;
  return <FormControl {...props} ><input type="checkbox" {...input} {...restProps} /></FormControl>
};

/* export const CheckBoxArea = ({ input, meta, ...props }) => {
  const hasError = meta.touched && meta.error;
  return (
    <div className={`formControl${hasError ? "-error" : ""}`}>
<label htmlFor="checkBox">{...input} {...props}</label>
<Control.checkbox {...input} {...props} />
   </div>
   );
 }; */