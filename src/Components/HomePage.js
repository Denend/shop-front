import React, { Component } from "react";
import SliderConteiner from "./ChildComponents/Slider/SliderConteiner";

class HomePage extends Component {
	render() {
		return (
			<div>
				<div className='HomePageSlider'>
					<SliderConteiner />
				</div>
				<div>
					<div className='brandContainer'>
						<article>
							<h3>Элитные бренды по самым низким ценам</h3>
						</article>
						<div className='BrandImage1'>
							<img
								src='https://upload.wikimedia.org/wikipedia/commons/thumb/6/69/Gap_logo.svg/1024px-Gap_logo.svg.png'
								alt=''
							></img>
						</div>
						<div className='BrandImage2'>
							<img
								src='https://shop4c60e634-309f-4061-b72d-b58582db3ae3.s3.amazonaws.com/LOGO-UsPolo?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAUOHTNFJ7U5SHUDG7%2F20191104%2Feu-central-1%2Fs3%2Faws4_request&X-Amz-Date=20191104T134929Z&X-Amz-Expires=604800&X-Amz-SignedHeaders=host&X-Amz-Signature=68f95fd0d1afb57d53a6b72e7e173750fc169c732dcf0d77f4d0f7fc14890d4b'
								alt=''
							></img>
						</div>
						<div className='BrandImag3'>
							<img
								src='https://shop4c60e634-309f-4061-b72d-b58582db3ae3.s3.amazonaws.com/LOGO-Francescas?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAUOHTNFJ7U5SHUDG7%2F20191104%2Feu-central-1%2Fs3%2Faws4_request&X-Amz-Date=20191104T134929Z&X-Amz-Expires=604800&X-Amz-SignedHeaders=host&X-Amz-Signature=342901d2be3267853a7e1ad4a0bfe566bfc5f68972d9fdc296e98af2a68b00ab'
								alt=''
							></img>
						</div>
						<div className='BrandImage4'>
							<img
								src='https://shop4c60e634-309f-4061-b72d-b58582db3ae3.s3.amazonaws.com/LOGO-BananaRepublic?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAUOHTNFJ7U5SHUDG7%2F20191104%2Feu-central-1%2Fs3%2Faws4_request&X-Amz-Date=20191104T134930Z&X-Amz-Expires=604800&X-Amz-SignedHeaders=host&X-Amz-Signature=7248c9b1e09469b6b45506eb6ba2e6b684ae2a3a5adba4a27bbb11caabf72793'
								alt=''
							></img>
						</div>
						<div className='BrandImage5'>
							<img
								src='https://shop4c60e634-309f-4061-b72d-b58582db3ae3.s3.amazonaws.com/LOGO-Boden?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAUOHTNFJ7U5SHUDG7%2F20191104%2Feu-central-1%2Fs3%2Faws4_request&X-Amz-Date=20191104T134930Z&X-Amz-Expires=604800&X-Amz-SignedHeaders=host&X-Amz-Signature=48bba8cf8850ceb858f86b5cbfa1a3522d896c31fcd5fde69d637708bdb1151e'
								alt=''
							></img>
						</div>
						<div className='BrandImage6'>
							<img
								src='https://shop4c60e634-309f-4061-b72d-b58582db3ae3.s3.amazonaws.com/LOGO-Bioware?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAUOHTNFJ7U5SHUDG7%2F20191104%2Feu-central-1%2Fs3%2Faws4_request&X-Amz-Date=20191104T134930Z&X-Amz-Expires=604800&X-Amz-SignedHeaders=host&X-Amz-Signature=32ea5bf9e4e1ca6d55a59237ab66af413079b5b9224c633235d6097704270c78'
								alt=''
							></img>
						</div>
						<div className='BrandImage7'>
							<img
								src='https://shop4c60e634-309f-4061-b72d-b58582db3ae3.s3.amazonaws.com/LOGO-Bethesda?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAUOHTNFJ7U5SHUDG7%2F20191104%2Feu-central-1%2Fs3%2Faws4_request&X-Amz-Date=20191104T134930Z&X-Amz-Expires=604800&X-Amz-SignedHeaders=host&X-Amz-Signature=a3e2caf6661b599e48887b25c8818e6bf8ed1e59ddd0205ba17d2634ade23399'
								alt=''
							></img>
						</div>
					</div>
				</div>
				<div className='HomePageDiscount'>{/* <HomePageAnim /> */}</div>
			</div>
		);
	}
}
export default HomePage;
