import React, { Component } from "react";
import { connect } from "react-redux";
import removeCartProduct from "../reduxActions/removeCartProduct";
import { withFormik } from "formik";
import placeOrder from "../reduxActions/placeOrder";
import OrderModal from "./ChildComponents/OrderModal";
import { toggleOrderModal } from "../reduxActions/toggleOrderModal";
import emptyCartImg from "./foto/empty-cart.png";

class Cart extends Component {
	handleRomoveClick = event => {
		const id = parseInt(event.currentTarget.id);
		const size = event.currentTarget.parentNode.id; //eslint-disable-line

		console.log(size);
		this.props.removeProduct(id, size);
	};
	calculatePrice = () => {
		const { products } = this.props;

		if (products.length)
			return products
				.map(currObj => {
					if (currObj.discount_price > 0) {
						return parseInt(currObj.discount_price);
					} else {
						return parseInt(currObj.price);
					}
				})
				.reduce((accumulator, currValue) => accumulator + currValue);
	};

	calculateQuantitiy = () => Object.keys(this.props.values).length;

	handleSumbit = e => {
		e.preventDefault();
		this.props.handleSubmit(e);
	};

	UrlToProductDesc = prodId => {
		this.props.history.push(`/shop/product/?article=${prodId}`); //passing product id throught url params;
	};

	render() {
		const {
			products,
			// handleChange,
			toggleModal,
			isModalOpen,
			values,
			placeOrder,
			orderErrorData,
		} = this.props;

		return products.length ? (
			<div className='mainCartContainer container-standart'>
				<div className='cart--productsAndOrderWrapper'>
					<div className='cart--header'>
						<h3>Ваша корзина</h3>
						<button className='placeOrderButton' onClick={toggleModal}>
							Оформить заказ
						</button>
					</div>
					<form
						id='cartForm'
						className='cart--productsWrapper'
						onSubmit={this.handleSumbit}
					>
						{products.map(
							(
								{
									article,
									images,
									brand,
									category,
									discount_price,
									price,
									sizes,
								},
								i,
							) => {
								const firstImage = images.length && images[0];
								return (
									<div className='product' key={i} id={i}>
										<div className='buttons' id={sizes[0]}>
											<span
												id={article}
												className='delete-btn'
												onClick={event => this.handleRomoveClick(event)}
											/>
										</div>
										<div className='image'>
											<img
												src={firstImage}
												alt={brand}
												id={article}
												onClick={e => this.UrlToProductDesc(e.currentTarget.id)}
											/>
											{/* to do : improve alt */}
										</div>
										<div className='description'>
											<span> {`${category} ${brand}`} </span>
											<p className='productArticle'>
												Артикул &#8470; {article}
											</p>
										</div>
										{/* <div className="quantity">
											<input
												id={article}
												type="number"
												min="1"
												name={article}
												defaultValue="1"
												onChange={e => {
													handleChange(e);
													e.target.parentElement.nextSibling.innerHTML =
														e.target.value * price + " ₴";
												}}
											/>
										</div> */}
										<div className='sizeContainer'>{sizes[0]}</div>
										<div
											className={`${
												discount_price > 0
													? "total-price-discounted"
													: "total-price"
											}`}
										>
											{" "}
											{discount_price > 0 ? discount_price : price} ₴
										</div>
									</div>
								);
							},
						)}
					</form>
					<div className='cart--footer'>
						<div className='footerContainer'>
							<div className='footer--note'>
								<p>Информация о заказе:</p>
							</div>
							<div className='footer--orderInfo'>
								<ul className='orderBulletpoints'>
									<li>Стоймость товаров:</li>
									<li>Стоймость доставки:</li>
									<li>Общая стоймость заказа:</li>
								</ul>
								<ul className='orderBillNumbers'>
									<li className='totalPrice'>{this.calculatePrice()}</li>
									<li className='deliveryCost'>Узнать</li>
									<li className='totalPrice'>{this.calculatePrice()}</li>
								</ul>
							</div>
						</div>
						<button className='placeOrderButton' onClick={toggleModal}>
							Оформить заказ
						</button>
						<OrderModal
							isModalOpen={isModalOpen}
							toggleModal={toggleModal}
							cartFormValues={values}
							placeOrder={placeOrder}
						/>
						{orderErrorData && orderErrorData.length ? (
							<div className='footer-orderError'>
								{orderErrorData.map(({ article, reason }, index) => (
									<li key={index}>
										Артикул &#8470; {article} - {reason}
									</li>
								))}
							</div>
						) : null}
					</div>
				</div>

				<aside className='cart--SideInfo'>
					<div className='SideInfo-header'>
						<div className='iconContainer'>
							<img
								src='https://cdn4.iconfinder.com/data/icons/shopping-in-color/64/shopping-in-color-05-512.png'
								alt=''
							/>
						</div>
						<p>Ваша корзина</p>
					</div>
					<hr />
					<div className='sideOrderInfo'>
						<ul className='orderBulletpoints'>
							<li>Количество:</li>
							<li>Стоймость товаров:</li>
							<li>Стоймость доставки:</li>
							<hr />
							<li>Всего:</li>
						</ul>
						<ul className='orderBillNumbers'>
							<li id='cartNum'> {this.calculateQuantitiy()} </li>
							<li className='totalPrice'>{this.calculatePrice()}</li>
							<li className='deliveryCost'>Узнать</li>
							<hr />
							<li className='total'>{this.calculatePrice()}</li>
						</ul>
					</div>
				</aside>
			</div>
		) : (
			<div className='emptyCartWrapper container-standart'>
				<img src={emptyCartImg} alt='empty cart'></img>
			</div>
		);
	}
}

const form = {
	enableReinitialize: true,
	mapPropsToValues: ({ products }) => {
		const initialValues = [];

		products.forEach(product => {
			const productData = {};
			productData.article = product.article;
			productData.size = product.sizes[0];
			initialValues.push(productData);
		});

		return initialValues;
	},
	// handleSubmit: (values, { props }) => {
	// 	const orderArticles = Object.keys(values);
	// 	props.placeOrder(orderArticles);
	// 	console.log("submitted");
	// },
	validateOnBlur: false,
	validateOnChange: false,
	isInitialValid: true,
};

const mapStateToProps = state => ({
	products: state.productCart.cartProducts,
	isModalOpen: state.productCart.isModalOpen,
	orderErrorData: state.cartProductStatus.notAvailable,
});

const mapDispatchToProps = dispatch => ({
	removeProduct: (productArticle, size) =>
		dispatch(removeCartProduct(productArticle, size)),
	placeOrder: (productsArray, userName, userPhoneNumber) =>
		dispatch(placeOrder(productsArray, userName, userPhoneNumber)),
	toggleModal: () => dispatch(toggleOrderModal()),
});
export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(withFormik(form)(Cart));

//just create a separate reducer and actios which are gonna pass an array to dispatch function
