import React, { useState, useEffect } from "react";
import SizesDropdown from "./SizesDropdown";
import { notyInfo } from "./noty";

const ShopProduct = ({ elem, index, onUpdateProduct, UrlToProductDesc }) => {
	const {
		article,
		images,
		brand,
		sizes,
		discount_price,
		price,
		category,
	} = elem;
	const [isProductHovered, setHovered] = useState(false);
	const [selectedSize, setSelectedSize] = useState(null);

	useEffect(() => {
		if (sizes.length === 1) {
			setSelectedSize(sizes[0].size);
		}
	}, [sizes]);

	const cleanActiveSize = event => {
		event.currentTarget.parentNode.parentNode
			.querySelector(".sizesDropdown-list")
			.childNodes.forEach(elem => elem.classList.remove("active"));
	};

	const handleHover = event => {
		event.stopPropagation();
		setHovered(true);
	};
	const handlePurchaseClick = event => {
		if (selectedSize) {
			onUpdateProduct(event, selectedSize);
			setHovered(false);
			setSelectedSize(null);
			sizes.length > 1 && cleanActiveSize(event);
		} else {
			setHovered(true);
			notyInfo("Выберите размер для покупки");
		}
	};
	const firstImage = images.length && images[0];
	return (
		<div
			className='Product'
			key={index}
			onMouseOver={event => handleHover(event)}
			onMouseOut={event => setHovered(false)}
		>
			<div className='Product-imageContainer'>
				<img
					id={article}
					src={firstImage}
					alt=''
					onClick={e => UrlToProductDesc(e.currentTarget.id)}
				/>

				{sizes.length > 1 && (
					<SizesDropdown
						sizes={sizes}
						isProductHovered={isProductHovered}
						setSelectedSize={setSelectedSize}
						selectedSize={selectedSize}
						cleanActiveSize={cleanActiveSize}
					/>
				)}
			</div>
			<div className='Product-infoWrapper'>
				<p className='productName'>{category + " " + brand}</p>
				<p className='sizes'>
					Размеры в наличии: {sizes.map(e => ` ${e.size} `)}
				</p>
				{discount_price > 0 && (
					<p className='discount'>{`${discount_price} ₴`}</p>
				)}
				<p
					className={`${discount_price > 0 ? "price-discounted" : "price"}`}
				>{`${price} ₴`}</p>

				<button type='button' name={article} onClick={handlePurchaseClick}>
					<div className='ButtonImgContainer'>
						<img
							src='https://cdn4.iconfinder.com/data/icons/shopping-in-color/64/shopping-in-color-05-512.png'
							alt=''
						/>
					</div>
					<div className='BuyButtonText'>Купить</div>
				</button>
			</div>
		</div>
	);
};

export default ShopProduct;
