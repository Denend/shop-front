import React, { Component } from "react";
import CheckBox from "../baseComponents/CheckBox";
import { withFormik } from "formik";
import { connect } from "react-redux";
import {
	resetFilterValues,
	setFilterValues
} from "../../reduxActions/setFilterValues";

class ShopFilters extends Component {
	handleSubmit = event => {
		const { submitForm } = this.props;
		event.preventDefault();
		submitForm();
	};

	handleChange = event => {
		const { handleChange, setFilterValues, handleSubmit, values } = this.props;
		setFilterValues(values);
		handleChange(event);
		handleSubmit();
	};
	handleReset = () => {
		const { resetFilterValues, toggleFilters } = this.props;
		resetFilterValues();
		toggleFilters();
	};

	render() {
		const { values, toggleFilters, isDesktop } = this.props;
		return (
			<div className="filterWrapperDiv">
				<form className="filterForm" onSubmit={this.handleSubmit}>
					{!isDesktop && (
						<a className="close" target="_self" onClick={() => toggleFilters()}>
							&times;
						</a>
					)}
					<div className="categoryFilterWraper">
						<h3>Категория товара</h3>
						<CheckBox
							id="scales1"
							name="category.tShirts"
							value={values.category.tShirts}
							checked={values.category.tShirts}
							label="Футболки"
							onChange={this.handleChange}
						/>

						<CheckBox
							id="scales2"
							name="category.hoodieAndSweaters"
							value={values.category.hoodieAndSweaters}
							checked={values.category.hoodieAndSweaters}
							label="Худи и кофты"
							onChange={this.handleChange}
						/>

						<CheckBox
							id="scales3"
							name="category.trouses"
							value={values.category.trouses}
							checked={values.category.trouses}
							label="Штаны"
							onChange={this.handleChange}
						/>
						<CheckBox
							id="scales4"
							name="category.jackets"
							value={values.category.jackets}
							checked={values.category.jackets}
							label="Куртки"
							onChange={this.handleChange}
						/>
						<CheckBox
							id="scales5"
							name="category.socksAndShirts"
							value={values.category.socksAndShirts}
							checked={values.category.socksAndShirts}
							label="Носки и майки"
							onChange={this.handleChange}
						/>
						<CheckBox
							id="scales6"
							name="category.dressesAndJumpsuits"
							value={values.category.dressesAndJumpsuits}
							checked={values.category.dressesAndJumpsuits}
							label="Платья и комбинезоны"
							onChange={this.handleChange}
						/>
					</div>
					<div className="brandFilterWraper">
						<h3>Бренд</h3>
						<CheckBox
							id="scales7"
							name="brand.Gap"
							value={values.brand.Gap}
							checked={values.brand.Gap}
							label="Gap"
							onChange={this.handleChange}
						/>
						<CheckBox
							id="scales8"
							name="brand.UsPolo"
							value={values.brand.UsPolo}
							checked={values.brand.UsPolo}
							label="U.S Polo"
							onChange={this.handleChange}
						/>
						<CheckBox
							id="scales9"
							name="brand.Francescas"
							value={values.brand.Francescas}
							checked={values.brand.Francescas}
							label="Francescas"
							onChange={this.handleChange}
						/>
						<CheckBox
							id="scales10"
							name="brand.BananaRepublic"
							value={values.brand.BananaRepublic}
							checked={values.brand.BananaRepublic}
							label="Banana Republic"
							onChange={this.handleChange}
						/>
						<CheckBox
							id="scales11"
							name="brand.Boden"
							value={values.brand.Boden}
							checked={values.brand.Boden}
							label="Boden"
							onChange={this.handleChange}
						/>
						<CheckBox
							id="scales12"
							name="brand.Bethesda"
							value={values.brand.Bethesda}
							checked={values.brand.Bethesda}
							label="Bethesda"
							onChange={this.handleChange}
						/>
						<CheckBox
							id="scales13"
							name="brand.Bioware"
							value={values.brand.Bioware}
							checked={values.brand.Bioware}
							label="Bioware"
							onChange={this.handleChange}
						/>
					</div>
					<div className="sizeFilterWraper">
						<h3>Размер</h3>
						<CheckBox
							id="scales14"
							name="size.XS"
							value={values.size.XS}
							checked={values.size.XS}
							label="XS"
							onChange={this.handleChange}
						/>
						<CheckBox
							id="scales15"
							name="size.S"
							value={values.size.S}
							label="S"
							checked={values.size.S}
							onChange={this.handleChange}
						/>
						<CheckBox
							id="scales16"
							name="size.M"
							value={values.size.M}
							label="M"
							checked={values.size.M}
							onChange={this.handleChange}
						/>
						<CheckBox
							id="scales17"
							name="size.L"
							value={values.size.L}
							label="L"
							checked={values.size.L}
							onChange={this.handleChange}
						/>
						<CheckBox
							id="scales18"
							name="size.XL"
							value={values.size.XL}
							checked={values.size.XL}
							label="XL"
							onChange={this.handleChange}
						/>
						<CheckBox
							id="scales19"
							name="size.XXL"
							value={values.size.XXL}
							checked={values.size.XXL}
							label="XXL"
							onChange={this.handleChange}
						/>
					</div>
				</form>
				{!isDesktop && (
					<div className="filterWrapperDiv-footer">
						<button onClick={() => toggleFilters()}>Показать результаты</button>
						<button onClick={this.handleReset}>Сбросить фильтры</button>
					</div>
				)}
			</div>
		);
	}
}
const form = {
	enableReinitialize: true,
	mapPropsToValues: ({ reduxFilterValues }) => {
		return { ...reduxFilterValues };
	},
	handleSubmit: (values, { props }) => {
		const { setFilterValues } = props;
		setFilterValues(values);
	},
	validateOnBlur: false,
	validateOnChange: false,
	isInitialValid: true
};

const mapDispatchToProps = dispatch => ({
	resetFilterValues: () => dispatch(resetFilterValues()),
	setFilterValues: updatedValues => dispatch(setFilterValues(updatedValues))
});

export default connect(
	null,
	mapDispatchToProps
)(withFormik(form)(ShopFilters));
