import React from "react";

const PaginationButton = props => {
	const { direction } = props;
	return (
		<button className="paginationButton">
			{direction === "left" ? "предидущая" : "следующая"}
		</button>
	);
};

export default PaginationButton;
