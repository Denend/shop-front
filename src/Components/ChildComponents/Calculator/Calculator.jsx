import React from "react";
import { Range, getTrackBackground } from "react-range";
import SelectComponet from "./Select";
import BasketItem from "./BasketItem";
import CategoriesForm from "./BasketPage/CategoriesForm";
import WhosaleForm from "./BasketPage/WhosaleForm";
import { CalculatorAPI } from "../../../reduxActions/calculatorAction";
import ButtonFormRedux from "./ButtonForm";
import { notyInfo } from "../noty.js";

const MIN = 5;
const MAX = 100;

class Calculator extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      values1: [5],
      price: null,
      rezult: [],
      basket: this.props.basket,
      selectedOption: null
    };
  }
  handleRomoveClick(Item) {
    this.props.deleteBasketItem(Item);
  }
  componentDidUpdate() {
    let weight = this.state.values1;

    this.props.setWeight(weight);
  }
  componentDidMount() {
    this.props.getCalculator();
  }

  getDataPrice = e => {
    const rezBody = e * this.state.values1;
    this.props.setResult(rezBody);
    this.props.setPrice(e);
    this.setState({ price: [e] });
    this.setState({ rezult: [rezBody] });
  };

  setSelectedOption = option => this.setState({ selectedOption: option });

  getDataLabel = e => {
    this.props.setBrend(e);
    this.props.togleIsFetching(true);
    this.props.setQBrand(e);
  };

  handleImageClick(event) {
    event.currentTarget.parentNode.childNodes.forEach(
      elem => elem.classList && elem.classList.remove("active")
    );
    event.currentTarget.classList.add("active");
    const clickedId = parseInt(event.currentTarget.id);
    const currentBrend = this.props.brends.filter(
      obj => obj.id === clickedId
    )[0];

    const sliderOption = {};
    sliderOption.value = currentBrend.name;
    sliderOption.label = currentBrend.name;
    sliderOption.price = currentBrend.price;
    this.getDataLabel(currentBrend.name);
    this.setSelectedOption(sliderOption);
    this.getDataPrice(sliderOption.price);
  }

  render() {
    const {
      isError,
      toggleError,
      isModalOpen,
      toggleModal,
      toggleGoBasket,
      price,
      weight,
      result,
      brend,
      brends,
      basket,
      isFetching,
      setResult,
      setWeight,
      setBasket,
      setCategoriesAPI,
      categoriesAPI
    } = this.props;

    let slide = e => {
      this.setState({ values1: e });
      let weight = this.state.values1;
      let result = this.state.price * this.state.values1;
      setResult(result);
      setWeight(weight);
    };

    let options = brends.map(b => ({
      value: b.name,
      label: b.name,
      price: b.price,
      qname: b.query_name
    }));

    let pushBasket = () => {
      let brendsFilter = basket.map(b => ({
        brand: b.brend
      }));

      if (brendsFilter.length > 0) {
        let brandss = brendsFilter.find(item => item.brand === brend);
        if (brandss !== undefined) {
          for (var key in brandss) {
            if (brend !== brandss[key]) {
              toggleError(true);
              toggleGoBasket(true);
              setBasket();
            } else {
              notyInfo("Выберите новый бренд");
            }
          }
        } else {
          toggleError(true);
          toggleGoBasket(true);
          setBasket();
        }
      } else {
        toggleError(true);
        toggleGoBasket(true);
        setBasket();
      }
    };

    let basketRend = basket.map(b => (
      <BasketItem
        basket={b}
        brend={b.brend}
        weight={b.weight}
        result={b.result}
        key={b.id}
        deleteCallback={this.handleRomoveClick.bind(this)}
        id={b.id}
      />
    ));
    Object.filter = (obj, predicate) =>
      Object.assign(
        ...Object.keys(obj)
          .filter(key => predicate(obj[key]))
          .map(key => ({ [key]: obj[key] }))
      );

    function isEmptyObject(obj) {
      for (var i in obj) {
        if (obj.hasOwnProperty(i)) {
          return false;
        }
      }
      return true;
    }
    function isObjectTrue(obj) {
      for (var i in obj) {
        if (obj[i] === true) {
          return true;
        }
      }
      return false;
    }
    const checkBasket = () => {
      if (basket.length !== 0) {
        toggleModal(true);
      } else {
        toggleError(false);
      }
    };
    const pushCategories = categories => {
      let isEmpty = isEmptyObject(categories);

      if (isEmpty === false) {
        if (isObjectTrue(categories) !== false) {
          let selected = Object.filter(
            categories,
            categoria => categoria !== false
          );
          setCategoriesAPI(Object.keys(selected));
          checkBasket();
        } else {
          notyInfo("Выберите категорию");
        }
      } else {
        notyInfo("Выберите категорию");
      }
    };

    const pushBasketState = item => {
      toggleModal(false);
      let order = basket.map(b => ({
        brand: b.brend,
        weight: Number(b.weight)
      }));

      CalculatorAPI.putBasket(
        order,
        categoriesAPI,
        item.phoneNumber,
        item.name,
        item.comment
      );
      // console.log(order, categoriesAPI, item.phoneNumber, item.name);
    };
    return (
      <div className="Wraper container-standart">
        <div className="brandImagesWraper">
          {brends.map(({ name, logo_url, id }, index) => (
            // <div
            //   className="imageWrapper"
            //   onClick={event => this.handleImageClick(event)}
            //   key={index}
            //   id={id}
            // >
            <img
              className="imageWrapper"
              id={id}
              src={logo_url}
              alt={name}
              onClick={event => this.handleImageClick(event)}
            />
            // </div>
          ))}
        </div>
        {isFetching ? (
          <div className="Range1">
            <Range
              step={1}
              min={5}
              max={100}
              values={this.state.values1}
              onChange={values1 => slide(values1)}
              renderTrack={({ props, children }) => (
                <div
                  onMouseDown={props.onMouseDown}
                  onTouchStart={props.onTouchStart}
                  style={{
                    ...props.style,
                    height: "36px",
                    display: "flex",
                    width: "100%"
                  }}
                >
                  <div
                    ref={props.ref}
                    style={{
                      height: "8px",
                      width: "100%",
                      borderRadius: "4px",
                      background: getTrackBackground({
                        values: this.state.values1,
                        colors: ["#4caf50", "#ccc"],
                        min: MIN,
                        max: MAX
                      }),
                      alignSelf: "center"
                    }}
                  >
                    {children}
                  </div>
                </div>
              )}
              renderThumb={({ props, isDragged }) => (
                <div
                  {...props}
                  style={{
                    ...props.style,
                    height: "42px",
                    width: "42px",
                    borderRadius: "4px",
                    backgroundColor: "#FFF",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    boxShadow: "3px 5px 7px #AAA"
                  }}
                >
                  <div
                    style={{
                      height: "16px",
                      width: "5px",
                      backgroundColor: isDragged ? "#4caf50" : "#CCC"
                    }}
                  />
                </div>
              )}
            />
          </div>
        ) : (
          <div className="Range1"></div>
        )}
        <div className="calculatorWraper">
          <div className="selectSectionContainer">
            <div className="selectWraper">
              <p>Выберите бренд</p>
              <SelectComponet
                options={options}
                getDataPrice={this.getDataPrice}
                getDataLabel={this.getDataLabel}
                selectedOption={this.state.selectedOption}
                setSelectedOption={this.setSelectedOption}
              />
            </div>
            {isFetching ? (
              <>
                <div className="checkboxWrapper">
                  <CategoriesForm
                    categories={this.props.categories}
                    onSubmit={pushCategories}
                    toggleModal={toggleModal}
                  />
                </div>
                <div className="livePurchaseInfo">
                  <div className="promRez">
                    <p className="promRez-header">Ваш заказ</p>
                    <p>Вес: {weight}кг</p>
                    <p>Цена за килограм: {price}₴ </p>

                    <p>Стоимость: {result}₴ </p>
                  </div>
                  <button type="button" onClick={pushBasket}>
                    <div className="ButtonImgContainer">
                      <img
                        src="https://cdn4.iconfinder.com/data/icons/shopping-in-color/64/shopping-in-color-05-512.png"
                        alt=""
                      />
                    </div>
                    <div className="BuyButtonText">Купить</div>
                  </button>
                </div>
              </>
            ) : (
              <div className="rezConteinerinfo">
                <div>Оформить оптовый заказ - это быстро и просто</div>
                <div>
                  Выбрать бренд -> Указать вес на ползунке -> Выбрать категорию
                  -> Добавить в корзину
                </div>
              </div>
            )}
          </div>
          {isFetching ? (
            <div className="calculatorWraperinfo">
              <div className="infoWraper ">
                <div className={`rezylt`}>
                  {isError ? (
                    <div>{basketRend}</div>
                  ) : (
                    <div className={"rezyltError"}>Добавте товар</div>
                  )}
                </div>
              </div>
              <div className="calculatorButtonsArea">
                <ButtonFormRedux
                  toggleModal={toggleModal}
                  onSubmit={pushCategories}
                  basket={basket}
                  toggleError={toggleError}
                  categoriesAPI={categoriesAPI}
                />
                <div className="form">
                  <WhosaleForm
                    isModalOpen={isModalOpen}
                    onSubmit={pushBasketState}
                    toggleModal={toggleModal}
                  />
                </div>
              </div>
            </div>
          ) : null}
        </div>
      </div>
    );
  }
}

export default Calculator;
