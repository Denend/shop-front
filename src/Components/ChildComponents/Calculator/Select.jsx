import React from "react";
import Select from "react-select";

class SelectComponet extends React.Component {
	handleChange = selectedOption => {
		const { setSelectedOption, getDataPrice, getDataLabel } = this.props;
		setSelectedOption(selectedOption);
		getDataPrice(selectedOption.price);
		getDataLabel(selectedOption.qname);
	};

	render() {
		let options = this.props.options;
		const { selectedOption } = this.props;

		return (
			<Select
				className='brandSelect'
				placeholder='Бренды'
				value={selectedOption}
				onChange={this.handleChange}
				options={options}
				valueKey={"price"}
			/>
		);
	}
}
export default SelectComponet;
