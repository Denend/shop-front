import React from "react";

const BasketItem = props => {
  let Callback = () => {
    props.deleteCallback(props.id);
  };
  
  return (
    <div className="mainBasketContainer ">
      <div className="basket--productsWrapper">
        <span className="basket--spanID">{props.id}.</span>
        <span className="basket--spanName"> {props.brend}</span>
        <span className="basket--span"> {props.weight}кг </span>
        <span className="basket--span"> {props.result}₴</span>
        <span id={props.id} className="basket--deleteButton" onClick={Callback}>
          X
        </span>
      </div>
    </div>
  );
};

export default BasketItem;
