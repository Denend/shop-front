import React from "react";
import { reduxForm, Field } from "redux-form";
import { numericality, length } from "redux-form-validators";
import { requiredField } from "../../validators/validators";
import { InputAreaNumber } from "../../../baseComponents/FormControls";
import { InputAreaName } from "../../../baseComponents/FormControls";

const WhosaleForm = props => {
  const {
    isModalOpen,
    toggleModal,
    submitting,
    handleSubmit
  } = props;
  const toggle = () => {
    toggleModal(true);
  };

  return (
    <div className={`orderModal${isModalOpen ? "-open" : "-closed"}`}>
      <div className="orderModal-header">
        <h3>Ваши контактные данные</h3>
        <button className="close" onClick={() => toggleModal(false)}>
        &times;
        </button>
     
      </div>
      <div className="orderModal-body">
        <form onSubmit={handleSubmit}>
          <div>
            <Field
              className="input"
              validate={[requiredField]}
              placeholder={"Ваше имя"}
              component={InputAreaName}
              name={"name"}
            />
          </div>
          <div>
            <Field
              className="input"
              validate={[
                requiredField,
                length({ is: 10 }),
                numericality({ int: true })
              ]}
              placeholder={"Ваш номер телефона"}
              component={InputAreaNumber}
              name={"phoneNumber"}
            />
          </div>
          <div>
            <Field
              className="input"
              placeholder={"Коментарий к заказу"}
              component={"input"}
              name={"comment"}
            />
          </div>
          <div>
            <button
              onClick={toggle}
              disabled={submitting}
            >
              Заказать
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};
const WhosaleReduxForm = reduxForm({
  form: "whosale"
})(WhosaleForm);
export default WhosaleReduxForm;
