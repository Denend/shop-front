import React from "react";
import CategoriesReduxForm from "./Categories";
import { reduxForm } from "redux-form";


const WhoSaleForm = props => {
  let categories = props.categories;
  let categoriesRend = categories.map(m => (
    <CategoriesReduxForm name={m.name} query_name={m.query_name} key={m.id} id={m.id} />
  ));
  return (
    <div >
     
        <p>Выберите категорию </p>
        {categoriesRend}
      
      
    </div>
  );
};
const CategoriesRedux = reduxForm({
  form: "categories"
})(WhoSaleForm);
export default CategoriesRedux;
