import React from "react";
import { reduxForm, Field } from "redux-form";


const CategoriesForm = props => {
	let names = props.query_name;
	return (
		<div className='checkboxArea'>
			<form onSubmit={props.handleSubmit}>
				<Field
					className='checkboxItem'
					id={props.id}
					component={"input"}
					type={"checkbox"}
					name={names}
				/>
				<label>{props.name}</label>
			</form>
		</div>
	);
};
const CategoriesReduxForm = reduxForm({
	form: "categories",
})(CategoriesForm);
export default CategoriesReduxForm;
