import { connect } from "react-redux";
import {
  toggleError,
  toggleGoBasket,
  toggleModal,
  setBrend,
  setQBrand,
  getCalculatorThunk,
  setBrends,
  setWeight,
  setResult,
  togleIsFetching,
  setPrice,
  setBasket,
  deleteBasketItem,
  setCategoriesAPI
} from "../../../reduxActions/calculatorAction";
import Calculator from "./Calculator";

let mapStateToProps = state => ({
  weight: state.calculatorComponent.weight,
  brends: state.calculatorComponent.brends,
  brend: state.calculatorComponent.brend,
  price: state.calculatorComponent.price,
  result: state.calculatorComponent.result,
  isFetching: state.calculatorComponent.isFetching,
  basket: state.calculatorComponent.basket,
  categories: state.calculatorComponent.categories,
  categoriesAPI: state.calculatorComponent.categoriesAPI,
  isModalOpen: state.calculatorComponent.isModalOpen,
  goBasket: state.calculatorComponent.goBasket,
  isError: state.calculatorComponent.isError,
});

export default connect(
  mapStateToProps,
  {
    toggleError,
    toggleGoBasket,
    toggleModal,
    setCategoriesAPI,
    setBrend,
    setQBrand,
    setBrends,
    setWeight,
    setResult,
    togleIsFetching,
    setPrice,
    setBasket,
    deleteBasketItem,
    getCalculator: getCalculatorThunk
  }
)(Calculator);
