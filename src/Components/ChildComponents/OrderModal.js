import React from "react";
import { withFormik } from "formik";
import * as Yup from "yup";

const OrderModal = props => {
	const {
		errors,
		touched,
		isModalOpen,
		toggleModal,
		values,
		handleChange,
		handleBlur,
		handleSubmit,
	} = props;
	const handleOrderClick = e => {
		handleSubmit(e);
		e.preventDefault();
	};

	return (
		<div className={`orderModal${isModalOpen ? "-open" : "-closed"}`}>
			<div className='orderModal-header'>
				<h3>Ваши контактные данные</h3>
				<button className="close" href="#" onClick={() => toggleModal(false)}> &times;</button>

			</div>
			<div className='orderModal-body'>
				<form onSubmit={handleSubmit}>
					<input
						type='name'
						className={`input${
							errors.name && touched.name ? "-incorrect" : ""
						}`}
						placeholder='Ваше имя'
						value={values.name}
						onChange={handleChange}
						onBlur={handleBlur}
						name='name'
					></input>
					{/* <input
						type="email"
						placeholder="Email"
						value={values.email}
						onChange={handleChange}
						onBlur={handleBlur}
						name="email"
					></input> */}
					<input
						type='tel'
						className={`input${
							errors.phone && touched.phone ? "-incorrect" : ""
						}`}
						placeholder='Ваш номер телефона'
						value={values.phone}
						onChange={handleChange}
						onBlur={handleBlur}
						name='phone'
					></input>
					{errors.name && touched.name ? (
						<span className='erroMassage'>{errors.name}</span>
					) : null}
					{errors.phone && touched.phone ? (
						<span className='erroMassage'>{errors.phone}</span>
					) : null}

					<button type='submit' onClick={e => handleOrderClick(e)}>
						<div>Оформить заказ</div>
					</button>
				</form>
			</div>
			<div className='orderModal-footer'></div>
		</div>
	);
};

const OrderSchema = Yup.object().shape({
	name: Yup.string()
		.min(2, "Слишком короткое имя")
		.max(40, "Слишком длинное имя")
		.required("Поле имя обязательное"),
	phone: Yup.string()
		.min(9, "Слишком короткий")
		.max(13, "Слишком длинный")
		.required("Поле номер телефона обязательное"),
});

const form = {
	enableReinitialize: true,
	mapPropsToValues: props => {
		const initialValues = { name: "", phone: "" };

		return { ...initialValues };
	},
	handleSubmit: (values, { props }) => {
		const orderArticles = props.cartFormValues;
		const { name, phone } = values;
		props.placeOrder(orderArticles, name, phone);
		props.toggleModal();
	},
	validationSchema: OrderSchema,
};

export default withFormik(form)(OrderModal);
