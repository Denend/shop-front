import React from "react";
import ImageGallery from "react-image-gallery";

const Gallery = ({ images }) => {
	const mapedImages =
		images &&
		images.map(image => {
			const gallerySlide = { original: image, thumbnail: image };
			return gallerySlide;
		});

	//  [
	// 	{
	// 		original:
	// 			"https://shop4c60e634-309f-4061-b72d-b58582db3ae3.s3.amazonaws.com/0b9eaa-Bethesda%2AL.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAUOHTNFJ7U5SHUDG7%2F20190921%2Feu-central-1%2Fs3%2Faws4_request&X-Amz-Date=20190921T153644Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=43c30ee025ab9c40b7d05206debd3686350abc9de7e6cabd8a871ca43b867f1a",
	// 		thumbnail:
	// 			"https://shop4c60e634-309f-4061-b72d-b58582db3ae3.s3.amazonaws.com/0b9eaa-Bethesda%2AL.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAUOHTNFJ7U5SHUDG7%2F20190921%2Feu-central-1%2Fs3%2Faws4_request&X-Amz-Date=20190921T153644Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=43c30ee025ab9c40b7d05206debd3686350abc9de7e6cabd8a871ca43b867f1a"
	// 	},
	// 	{
	// 		original: "https://picsum.photos/id/1015/1000/600/",
	// 		thumbnail: "https://picsum.photos/id/1015/250/150/"
	// 	},
	// 	{
	// 		original: "https://picsum.photos/id/1019/1000/600/",
	// 		thumbnail: "https://picsum.photos/id/1019/250/150/"
	// 	}
	// ];

	return <ImageGallery items={mapedImages} thumbnailPosition="left" />;
};

export default Gallery;
