import React from "react";
import { Link } from "react-router-dom";

const OrderDone = (props) => {
  return (
    <div className="orderDoneWraper container-standart">
      <div className="doneTextArea">
        <p>Спасибо что выбрали наш магазин</p>
        <p>Ваш заказ оформлен и обрабатывается операторами</p>
        <p>Хорошего дня =)</p>
      </div>
      <div className="whosaleButtonsArea">
      <Link to="/home">
       
          <button>
            <div className="BuyButtonText"> На главную</div>
          </button>
       
      </Link>
       </div>
    </div>
  );
};
export default OrderDone;
