import React from "react";
// import { withRouter } from "react-router";
// import { connect } from "react-redux";
import { NavLink } from "react-router-dom";
import logo from "../foto/1.png";

const NavbarUpper = ({
	isDesktop,
	changeLocationCart,
	setMenuHeight,
	menuHeight,
	products,
}) => (
	<div className='NavbarDiv-upper'>
		<ul>
			<li>
				<NavLink to='/paymentAndDelivery' activeClassName='activeLink'>
					Оплата и доставка
				</NavLink>
			</li>
			<li>
				<NavLink to='/contact' activeClassName='activeLink'>
					Контакты
				</NavLink>
			</li>
			<li className='CartAndMenuContainer'>
				{!isDesktop && (
					<div
						id='nav-icon1'
						onClick={() => setMenuHeight(menuHeight === 60 ? "auto" : 60)}
					>
						<span></span>
						<span></span>
						<span></span>
					</div>
				)}
				{!isDesktop && (
					<div className='logoContainer'>
						<NavLink to='/home' activeClassName='activeLink'>
							<img src={logo} alt='logo'></img>
						</NavLink>
					</div>
				)}
				{isDesktop && (
					<NavLink
						to='/cart'
						onClick={changeLocationCart}
						activeClassName='activeLink'
					>
						Корзина покупок
					</NavLink>
				)}
				<div className='CartIconWrapper' onClick={changeLocationCart}>
					<img
						src='https://i.ya-webdesign.com/images/buy-vector-background-17.png'
						alt=''
					></img>
					<p id='cartNum'>{products && products.length}</p>
				</div>
			</li>
		</ul>
	</div>
);
export default NavbarUpper;
