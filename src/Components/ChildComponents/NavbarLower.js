import React from "react";
// import { withRouter } from "react-router";
// import { connect } from "react-redux";
import { NavLink } from "react-router-dom";
import logo from "../foto/1.png";

const NavbarLower = ({ isDesktop }) => (
	<div className='NavbarDiv-lower'>
		{isDesktop && (
			<div className='logoContainer'>
				<NavLink to='/home' activeClassName='activeLink'>
					<img src={logo} alt='logo'></img>
				</NavLink>
			</div>
		)}
		<ul>
			<li>
				<NavLink to='/home' activeClassName='activeLink'>
					Главная
				</NavLink>
			</li>
			<li>
				<NavLink to='/shop' activeClassName='activeLink'>
					Магазин
				</NavLink>
			</li>
			<li>
				<NavLink to='/wholesaleCalculator' activeClassName='activeLink'>
					Оптовые продажи
				</NavLink>
			</li>
			<li>
				<NavLink to='/about' activeClassName='activeLink'>
					О нас
				</NavLink>
			</li>

			{/* <li>
								<NavLink to="/">English</NavLink>
							</li> */}
		</ul>
	</div>
);

export default NavbarLower;
