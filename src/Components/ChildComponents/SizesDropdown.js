import React, { useState, useEffect } from "react";

const SizesDropdown = ({
	sizes,
	isProductHovered,
	setSelectedSize,
	cleanActiveSize,
}) => {
	const [dropdownHeight, setDropdownHeight] = useState(0);
	useEffect(() => {
		if (isProductHovered) {
			setDropdownHeight(dropdownHeight === 0 ? "auto" : 0);
		}
	}, [isProductHovered]); // eslint-disable-line

	// const cleanActiveSize = () => {
	// 	Array.from(
	// 		document.querySelector(".sizesDropdown-list").childNodes,
	// 	).forEach(elem => elem.classList.remove("active"));
	// 	console.log(document.querySelector(".sizesDropdown-list").childNodes);

	// };

	const handleSizeClick = e => {
		cleanActiveSize(e);
		e.currentTarget.classList.add("active");
		setSelectedSize(e.target.id);
	};

	return (
		sizes.length && (
			<div className={`sizesDropdown${isProductHovered ? "-active" : ""}`}>
				<ul className='sizesDropdown-list'>
					{sizes.map(({ size }, index) => (
						<li key={index} id={size} onClick={event => handleSizeClick(event)}>
							{size}
						</li>
					))}
				</ul>
			</div>
		)
	);
};

export default SizesDropdown;
