import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slide from "./Slide";

const TEST = props => {
	var settings = {
		dots: true,
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 2000,
		pauseOnHover: true,
	};
	let slideElement = props.products_data.map(m => (
		<Slide products_data={m} key={m.id} />
	));
	return (
		<div className='sliderWraper  '>
			<h2>Акционные предложения</h2>

			<Slider {...settings}>{slideElement}</Slider>
		</div>
	);
};

export default TEST;
