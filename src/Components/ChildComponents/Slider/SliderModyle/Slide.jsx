import React from "react";
import { withRouter } from "react-router";

const Slide = props => {
	const { products_data } = props;
	const UrlToProductDesc = prodId => {
		props.history.push(`/shop/product/?article=${prodId}`);
	};

	const firstImage = products_data.images.length && products_data.images[0];
	return (
		<div
			className='SlideBox'
			id={products_data.article}
			onClick={e => UrlToProductDesc(e.currentTarget.id)}
		>
			<img src={firstImage} alt='Slide' />
			<p className='dicond'> {products_data.discount_price} ₴</p>

			<p className='price'> {products_data.price} ₴</p>
		</div>
	);
};

export default withRouter(Slide);
