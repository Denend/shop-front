import React from "react";
import { connect } from "react-redux";
import {
	setproducts_dataAC,
	togleIsFetchingAC
} from "../../../reduxActions/sliderAction";
import Preloder from "./Preloader/Preloder";
import * as axios from "axios";
import Slider from "../Slider/SliderModyle/Slider";
import baseUrl from "../../../lib/baseUrl";

class SliderClass extends React.Component {
	componentDidMount() {
		this.props.togleIsFetching(true);

		axios.get(`${baseUrl}products?per_page=6&discount=1`).then(response => {
			this.props.setproducts_data(response.data.products_data);
			this.props.togleIsFetching(false);
		});
	}

	render() {
		return (
			<>
				<div>
					{this.props.isFetching ? <Preloder /> : null}
					<Slider products_data={this.props.products_data} />
				</div>
			</>
		);
	}
}

let mapStateToProps = state => ({
	products_data: state.sliderPage.products_data,
	isFetching: state.sliderPage.isFetching
});

let mapDispatchToProps = dispatch => ({
	setproducts_data: products_data => {
		dispatch(setproducts_dataAC(products_data));
	},
	togleIsFetching: isFetching => {
		dispatch(togleIsFetchingAC(isFetching));
	}
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(SliderClass);
