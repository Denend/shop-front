import React, { useState, useEffect } from "react";
import Select from "react-select";
import { connect } from "react-redux";
import { setFilterValues } from "../../reduxActions/setFilterValues";
import { initialState } from "../../reducers/filterProducts";

const SelectFilter = ({
	reduxValues,
	filterType,
	setFilterValues,
	className
}) => {
	const [currentOption, setCurrentOption] = useState(null);
	const placeholder = `Фильтровать по ${
		filterType === "price"
			? "цене"
			: filterType === "date"
			? "дате"
			: "хую носорога"
	}`;

	const handleChange = selectedOption => {
		setCurrentOption(selectedOption);
		const selectValues = { ...initialState[filterType] };
		selectValues[selectedOption.value] = true;
		const updatedFilterValues = {};
		updatedFilterValues[filterType] = selectValues;
		setFilterValues(updatedFilterValues);
		setCurrentOption(selectedOption);
		console.log(selectedOption);
	};

	const checkIfSorted = () => {
		let sorted = null;
		for (const key in reduxValues[filterType]) {
			if (reduxValues[filterType][key]) {
				sorted = true;
			}
		}
		return sorted;
	};

	useEffect(() => {
		if (!checkIfSorted()) setCurrentOption(null);
	}, [checkIfSorted, reduxValues]);

	const Labels = {
		price: {
			ascend: "От дешевого к дорогому",
			descend: "От дорогого к дешевому"
		},
		date: {
			ascend: "От старого к новому",
			descend: "От нового к старому"
		}
	};

	const filterTypeSection = Object.entries(reduxValues[filterType]);

	const options = filterTypeSection.map(elem => {
		const oneOptionObj = {};
		const elemKey = elem[0];
		oneOptionObj.value = elemKey;
		oneOptionObj.label = Labels[filterType][elemKey];
		return oneOptionObj;
	});

	return (
		<Select
			className={className}
			value={currentOption}
			onChange={handleChange}
			options={options}
			placeholder={placeholder}
			isSearchable={false}
		/>
	);
};

const mapDispatchToProps = dispatch => ({
	setFilterValues: updatedValues => dispatch(setFilterValues(updatedValues))
});

export default connect(
	null,
	mapDispatchToProps
)(SelectFilter);
