const initialState = {
	isDesktop: true
};
const breakpointsReducer = (state = initialState, { type, payload }) => {
	if (type === "SET_BREAKPOINT") {
		return { ...initialState, isDesktop: payload };
	} else return state;
};

export default breakpointsReducer;
