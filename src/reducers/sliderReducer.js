const TOGLE_IS_FETCHING = "TOGLE_IS_FETCHING";

const SET_USERS = "SET_USERS";
const SET_CURRENT_PAGE = "SET_CURRENT_PAGE";

let initialState = {
  products_data: [],
  pageSize: 5,
  totalPagesCount: 54,
  currentPage: 0,
  isFetching: false
};

const sliderReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_USERS: {
      return { ...state, products_data: action.products_data };
    }
    case SET_CURRENT_PAGE: {
      return { ...state, currentPage: action.currentPage };
    }

    case TOGLE_IS_FETCHING: {
      return { ...state, isFetching: action.isFetching };
    }
    default:
      return state;
  }
};
export const setproducts_dataAC = products_data => ({
  type: SET_USERS,
  products_data
});
export const setcurrentPageAC = currentPage => ({
  type: SET_CURRENT_PAGE,
  currentPage: currentPage
});
export const togleIsFetchingAC = isFetching => ({
  type: TOGLE_IS_FETCHING,
  isFetching: isFetching
});
export default sliderReducer;
