const initialState = {
	fetching: false,
	fetched: false,
	goodsArray: [],
	error: null
};
export default function fetchProductsReducer(
	state = initialState,
	{ type, payload }
) {
	switch (type) {
		case "FETCH_GOODS_START": {
			return { ...state, fetching: true };
		}
		case "FETCH_GOODS_COMPLETED": {
			return { ...state, fetched: true, fetching: false, goodsArray: payload };
		}
		case "FETCH_GOODS_ERR": {
			return { ...state, fetching: false, error: payload };
		}
		default:
			return state;
	}
}
