export default function productsReducer(
	initState = {
		cartProducts: [],
		orderInfo: {},
		notAvailable: [],
		isModalOpen: false,
	},
	{ type, payload },
) {
	switch (type) {
		case "ADD_PRODUCT_TO_CART":
			return {
				...initState,
				cartProducts: [...initState.cartProducts, ...payload],
			};
		case "REMOVE_PRODUCT_FROM_CART": {
			return { ...initState, ...payload };
		}
		case "PLACE_ORDER": {
			return { ...initState, ...payload };
		}
		case "PLACE_ORDER_ERROR": {
			return { ...initState, ...payload };
		}
		case "TOGGLE_CART_MODAL": {
			return { ...initState, isModalOpen: payload };
		}
		default:
			return initState;
	}
}
