export const initialState = {
	brand: {
		BananaRepublic: false,
		Bethesda: false,
		Bioware: false,
		Boden: false,
		Francescas: false,
		Gap: false,
		UsPolo: false
	},
	size: {
		L: false,
		M: false,
		S: false,
		XL: false,
		XS: false,
		XXL: false
	},
	category: {
		dressesAndJumpsuits: false,
		hoodieAndSweaters: false,
		jackets: false,
		socksAndShirts: false,
		tShirts: false,
		trouses: false
	},
	price: {
		ascend: false,
		descend: false
	},
	date: {
		ascend: false,
		descend: false
	}
};
export function filterProducts(state = initialState, { type, payload }) {
	switch (type) {
		case "SET_FILTER_VALUES": {
			return Object.assign({}, state, {
				...payload
			});
		}
		case "FILTER_PRODUCTS_START": {
			return { ...state };
		}
		case "FILTER_PRODUCTS_COMPLETED": {
			return { ...state, updatedValues: payload };
		}
		case "FILTER_PRODUCTS_ERR": {
			return { ...state };
		}
		case "RESET_FILTERS": {
			return { ...state, ...initialState };
		}
		default:
			return state;
	}
}
