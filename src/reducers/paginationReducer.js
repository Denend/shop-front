const initialState = {
	currentPage: 1
};
const paginationReducer = (state = initialState, { type, payload }) => {
	switch (type) {
		case "RECORD_CURRENT_PAGE": {
			return { ...state, ...payload };
		}
		default: {
			return state;
		}
	}
};

export default paginationReducer;
