const initialState = {
	fetching: false,
	fetched: false,
	productInfo: {},
	error: null
};
export default function productInfoReducer(
	state = initialState,
	{ type, payload }
) {
	switch (type) {
		case "FETCH_INFO_START": {
			return { ...state, fetching: true };
		}
		case "FETCH_INFO_COMPLETED": {
			return { ...state, fetched: true, fetching: false, productInfo: payload };
		}
		case "FETCH_INFO_ERR": {
			return { ...state, fetching: false, error: payload };
		}
		default:
			return state;
	}
}
