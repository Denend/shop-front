const TOGLE_IS_FETCHING = "TOGLE_IS_FETCHING";
const GO_BASKET = "GO_BASKET";
const SET_BREND = "SET_BREND";
const SET_Q_BRAND = "SET_Q_BRAND";
const SET_PRICE = "SET_PRICE";
const SET_BRENDS = "SET_BRENDS";
const SET_WEIGHT = "SET_WEIGHT";
const SET_REZULT = "SET_REZULT";
const SET_BASKET = "SET_BASKET";
const SET_CATEGORIES_API = "SET_CATEGORIES_API";
const SET_CATEGORIES = "SET_CATEGORIES";
const DELETE_BASKET_ITEM = "DELETE_BASKET_ITEM";
const TOGLE_IS_MODAL = "TOGLE_IS_MODAL";
const TOGLE_IS_ERROR = "TOGLE_IS_ERROR";

let initialState = {
  brends: [],
  categories: [],
  weight: null,
  result: null,
  brend: null,
  price: null,
  isError: false,
  isFetching: false,
  goBasket: false,
  basket: [],
  categoriesAPI: [],
  qBrand: null,
  isModalOpen: false
};

const calculatorReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_BRENDS: {
      return { ...state, brends: action.brends };
    }
    case SET_CATEGORIES: {
      return { ...state, categories: action.categories };
    }
    case SET_BREND: {
      return { ...state, brend: action.brend };
    }
    case SET_Q_BRAND: {
      return { ...state, qBrand: action.qBrand };
    }
    case SET_PRICE: {
      return { ...state, price: action.price };
    }
    case SET_WEIGHT: {
      return { ...state, weight: action.weight };
    }
    case SET_REZULT: {
      return { ...state, result: action.result };
    }
    case SET_CATEGORIES_API: {
      return { ...state, categoriesAPI: action.categoriesAPI };
    }
    case TOGLE_IS_FETCHING: {
      return { ...state, isFetching: action.isFetching };
    }
    case TOGLE_IS_ERROR: {
      return { ...state, isError: action.isError };
    }
    case GO_BASKET: {
      return { ...state, goBasket: action.goBasket };
    }
    case TOGLE_IS_MODAL: {
      return { ...state, isModalOpen: action.isModalOpen };
    }
    case DELETE_BASKET_ITEM: {
      return { ...state, ...action.payload };
    }

    case SET_BASKET: {
      let brend = state.brend;
      let qBrand = state.qBrand;
      let weight = state.weight;
      let result = state.result;
      return {
        ...state,
        basket: [
          ...state.basket,
          {
            id: state.basket.length + 1,
            brend: brend,
            weight: weight,
            result: result,
            qBrand: qBrand
          }
        ]
      };
    }

    default:
      return state;
  }
};

export default calculatorReducer;
