export default function cartErrorReducer(
	initState = {
		notAvailable: [],
	},
	{ type, payload },
) {
	switch (type) {
		case "PLACE_ORDER_ERROR": {
			return { ...initState, ...payload };
		}
		default:
			return initState;
	}
}
