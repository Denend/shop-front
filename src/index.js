import React from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { combineReducers, createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import { persistStore, persistReducer } from "redux-persist";
import { PersistGate } from "redux-persist/integration/react";
import storage from "redux-persist/lib/storage";
import productsReducer from "./reducers/productReducer";
import logger from "redux-logger";
import thunk from "redux-thunk";
import fetchProductsReducer from "./reducers/fetchProductsReducer";
import { filterProducts } from "./reducers/filterProducts";
import sliderReduser from "./reducers/sliderReducer";
import paginationReducer from "./reducers/paginationReducer";
import productInfoReducer from "./reducers/productInfoReducer";
import calculatorReducer from "./reducers/calculatorReducer";
import breakpointsReducer from "./reducers/breakpointsReducer";
import cartErrorReducer from "./reducers/cartErrorReducer";
import screenResize from "./reduxActions/screenResize";
import { reducer as formReducer } from "redux-form";

const allReducers = combineReducers({
	productCart: productsReducer,
	shopProducts: fetchProductsReducer,
	updatedFilterValues: filterProducts,
	sliderPage: sliderReduser,
	pagination: paginationReducer,
	productInfo: productInfoReducer,
	calculatorComponent: calculatorReducer,
	form: formReducer,
	breakpoints: breakpointsReducer,
	cartProductStatus: cartErrorReducer,
});

const persistConfig = {
	key: "root",
	storage,
	whitelist: ["productCart"],
};

const persistedReducer = persistReducer(persistConfig, allReducers);

const store = createStore(persistedReducer, applyMiddleware(thunk, logger));
const persistor = persistStore(store);
window.addEventListener("resize", () => {
	store.dispatch(screenResize(window.innerWidth));
});
store.dispatch(screenResize(window.innerWidth));

ReactDOM.render(
	<Provider store={store}>
		<PersistGate loading={null} persistor={persistor}>
			<App />
		</PersistGate>
	</Provider>,
	document.getElementById("root"),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
